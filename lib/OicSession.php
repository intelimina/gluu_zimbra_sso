<?php
require_once __DIR__ . '/common.php';

class OicSession extends ActiveRecord\Model {
	const DATETIME_FORMAT = 'Y-m-d H:i:s';
	
	static $before_create = array(
		'createTimestamp',
	);
	static $before_save = array(
		'updateTimestamp',
		'randomizeState',
		'randomizeNonce',
	);

	// scopes and predicates
	public function isExpired() { return $this->attributes()['expires_at'] < self::getTimestamp(); }
	public function isIncomplete() { return empty($this->attributes()['access_token']); }
	public function isComplete() { return !empty($this->attributes()['access_token']); }
	
	// helpers
	public static function getTimestamp($datetime = NULL) {
		$now = new DateTime($datetime);
		return $now->format(self::DATETIME_FORMAT);
	}
	
	public static function getClientConfig($name = NULL) {
		$app = getApp();
		
		if ($name == NULL) {
			return $app->getConfig()['openid_connect'];
		} else {
			return $app->getConfig()['openid_connect'][$name];
		}
	}

	public static function requestDynamicConfig() {
		$client_config = self::getClientConfig();
		
		$config_path = '/.well-known/openid-configuration';
		$config_url = $client_config['url'] . $config_path;
	
		$ch = curl_init($config_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		if (!empty($client_config['insecure'])) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		}
		$response = curl_exec($ch);
		if (curl_errno($ch)) {
			throw new OicConnectionException(curl_strerror(curl_errno($ch)));
		}
	
		$json_response = json_decode($response, TRUE);
		if (json_last_error()) {
			throw new OicGarbageException(json_last_error_msg() . " while retrieving dnyamic config");
		}
		
		return $json_response;
	}
	
	public static function requestToken($query) {
		$token_endpoint = self::getDynamicConfig('token_endpoint');
		$client_id = self::getClientConfig('client_id');
		$client_secret = self::getClientConfig('client_secret');
		
		$urlsafe_data = http_build_query($query);
		
		$ch = curl_init($token_endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $urlsafe_data);
		curl_setopt($ch, CURLOPT_USERPWD, "$client_id:$client_secret");
		if (self::getClientConfig('insecure')) {
			curl_setopt($ch, CURLOPT_SSL_VERIFY_PEER, FALSE);
		}
		$response = curl_exec($ch);
		
		if (curl_errno($ch)) {
			throw new OicConnectionException(curl_strerror(curl_errno($ch)));
		}
		
		$json_response = json_decode($response, TRUE);
		if (json_last_error()) {
			throw new OicGarbageException(json_last_error_msg() . " while retrieving token.");
		}
		
		if (!empty($json_response['error'])) {
			throw new OicErrorException("Authorization server returned " . $json_response["error"] . ": " . $json_response['error_description']);
		}
		
		return $json_response;
	}

	public function getAccessToken() {
		$query = $this->getAccessTokenQuery();
		$response = self::requestToken($query);
		
		$this->set_attributes(['access_token' => $response['access_token']]);
		if (!empty($response['refresh_token'])) {
			$this->set_attributes(['refresh_token' => $response['refresh_token']]);
		}
		if (!empty($response['id_token'])) {
			$this->set_attributes(['id_token' => $response['id_token']]);
		}
		if (!empty($response['expires_in'])) {
			$this->set_attributes(['expires_at' => self::getTimestamp('now + ' . $response['expires_in'] . ' seconds')]);
		}
		return $response;
	}

	public function refreshAccessToken() {
		if (empty($this->access_token)) {
			throw new OicClientException('Attempted to refresh token without an access token');
		}
		if (empty($this->refresh_token)) {
			throw new OicClientException('Attempted to refresh token without a refresh token');
		}
		if (empty($this->id_token)) {
			throw new OicClientException('Attempted to refresh token without an ID token');
		}

		$query = $this->getAccessTokenRefreshQuery();
		$response = self::requestToken($query);
		
		$this->set_attributes(['access_token' => $response['access_token']]);
		if (!empty($response['refresh_token'])) {
			$this->set_attributes(['refresh_token' => $response['refresh_token']]);
		}
		if (!empty($response['id_token'])) {
			$this->set_attributes(['id_token' => $response['id_token']]);
		}
		if (!empty($response['expires_in'])) {
			$this->set_attributes(['expires_at' => self::getTimestamp('now + ' . $response['expires_in'] . ' seconds')]);
		}
		return $response;
	}

	public static function getDynamicConfig($name = NULL) {
		$client_config = self::getClientConfig();
		$hash = sha1(json_encode($client_config));
		
		// check if we copied the dynamic config to the config file
		// if so, don't bother retrieving from the server
		if (!empty($client_config['dynamic'])) {
			$dynamic_config = $client_config['dynamic'];
		}
		
		// check if dynamic config is in app cache (to minimize retrieving from server)
		if (empty($dynamic_config)) {
			$app = getApp();
			$dynamic_config = $app->getCacheItem("oic_session_dynamic_{$hash}");
			
			if (empty($dynamic_config)) {
				// retrieve and cache the dynamic configuration
				$dynamic_config = self::requestDynamicConfig();
				if (empty($client_config['dynamic_config_expiry'])) {
					$expiry = 86400;
				} else {
					$expiry = $client_config['dynamic_config_expiry'];
				}
				$app->setCacheItem("oic_session_dynamic_{$hash}", $dynamic_config, $expiry);
			}
		}
		
		// return the config
		if ($name == NULL) {
			return $dynamic_config;
		} else {
			return $dynamic_config[$name];
		}
	}

	public function getAuthorizationUrl() {
		$query = $this->getAuthorizationQuery();
		$urlsafe_query = http_build_query($query);
		
		return self::getDynamicConfig('authorization_endpoint') . '?' . $urlsafe_query;
	}
	
	public function getEndSessionUrl() {
		$query = $this->getEndSessionQuery();
		$urlsafe_query = http_build_query($query);
		
		return self::getDynamicConfig('end_session_endpoint') . '?' . $urlsafe_query;
	}
	
	public function getAuthorizationQuery() {
		if (empty($this->state)) { $this->randomizeState(); }
		if (empty($this->nonce)) { $this->randomizeNonce(); }
		
		return array(
			'response_type' => 'code',
			'redirect_uri' => getApp()->getConfig('baseurl') . '/gluu/login.php',
			'scope' => 'openid email',
			'client_id' => self::getClientConfig('client_id'),
			'state' => $this->state,
			'nonce' => $this->nonce,
		);
	}
	
	public function getAccessTokenQuery() {
		return array(
			'grant_type' => 'authorization_code',
			'code' => $this->code,
			'scope' => 'openid email',
			'id_token' => $this->id_token,
			'redirect_uri' => getApp()->getConfig('baseurl') . '/gluu/login.php',
		);
	}
	
	public function getAccessTokenRefreshQuery() {
		return array(
			'grant_type' => 'refresh_token',
			'refresh_token' => $this->refresh_token,
			'scope' => 'openid email',
		);
	}

	public function getEndSessionQuery() {
		return array(
			'id_token_hint' => $this->id_token,
			'session_state' => $this->session_state,
			'post_logout_redirect_uri' => getApp()->getConfig('baseurl') . '/gluu/logout.php',
		);
	}

	public function createTimestamp() {
		if (empty($this->attributes()['created_at'])) {
			$this->set_attributes(['created_at' => self::getTimestamp()]);
		}
	}
	
	public function updateTimestamp() {
		$this->set_attributes(['updated_at' => self::getTimestamp()]);
	}

        public function validateIdToken() {
                // validate JWT
                $jwt = new JWT($this->attributes()['id_token']);
                $validation_params = array();
                if (!empty($this->nonce)) {
                        if ($this->nonce != $jwt->getClaim('nonce')) {
                                return false;
                        }
                }
                $validation_params['secret'] = self::getClientConfig('client_secret');
                $validation_params['aud'] = self::getClientConfig('client_id');
                $validation_params['exp'] = TRUE;
                $validation_params['nbt'] = TRUE;
                return $jwt->validate($validation_params);

                return true;
        }

        public function getClaims() {
                $jwt = new JWT($this->attributes()['id_token']);
                return $jwt->getClaims();
        }

        public function getClaim($name) {
                return $this->getClaims()[$name];
        }

	public function randomizeState() {
		if (empty($this->attributes()['state'])) {
			$this->set_attributes(['state' => base64_encode(openssl_random_pseudo_bytes(36))]);
		}
	}
	
	public function randomizeNonce() {
		if (empty($this->attributes()['nonce'])) {
			$this->set_attributes(['nonce' => base64_encode(openssl_random_pseudo_bytes(36))]);
		}
	}
}
