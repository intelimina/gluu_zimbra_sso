<?php
require_once __DIR__ . '/App.php';
function getApp() {
	static $app = NULL;
	
	if (empty($app)) {
		$app = new App(dirname(dirname(__FILE__)) . '/config/config.php');
	}
	
	return $app;
}

function appUrl($path = NULL) {
	if (!empty($path) && $path[0] != '/') {
		$path = '/' . $path;
	}
	return getApp()->getConfig('baseurl') . $path;
}

function render($path) {
	include dirname(__DIR__) . '/views/' . $path;
}
