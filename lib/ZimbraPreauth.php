<?php
require_once __DIR__ . '/common.php';

class ZimbraPreauth {
	public function __construct($account, $expiry=0) {
		$this->account = $account;
		$this->timestamp = (new DateTime())->format('U') * 1000;
		$this->expiry = $expiry;
	}
	
	public static function getZimbraConfig($name = NULL) {
		$app = getApp();
		
		if ($name == NULL) {
			return $app->getConfig()['zimbra'];
		} else {
			return $app->getConfig()['zimbra'][$name];
		}
	}
	
	public function getRedirectUrl() {
		return self::getZimbraConfig('url') . '/service/preauth?' .
		           http_build_query($this->getRedirectQuery());
	}
	
	public function getRedirectQuery() {
		$query = [
			'account' => $this->account,
			'by' => 'name',
			'expires' => $this->expiry,
			'timestamp' => $this->timestamp,
		];
		$key = self::getZimbraConfig('preauth_key');
		$preauth = hash_hmac('sha1', join('|', $query), $key);
		$query['preauth'] = $preauth;
		return $query;
	}
}

