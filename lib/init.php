<?php
// initialize the app with the config file
require_once dirname(__FILE__) . '/App.php';

// globlaly required libraries
$requires = array(
	'common',
	'OicException',
	'OicSession',
	'JWTException',
	'JWT',
	'ZimbraPreauth',
);

foreach ($requires as $file) {
	require_once dirname(__FILE__) . '/' . $file . '.php';
}

$app = getApp();
