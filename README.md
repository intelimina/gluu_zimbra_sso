Gluu/Zimbra SSO connector
=========================

This middleware serves as a connector between Zimbra pre-authentication and OpenID Connect 1.0.

It has been tested on
- Zimbra 8.6.0 Community
- PHP 5.3 and higher
  - curl extension
  - PDO extension
  - OpenSSL


## OpenID Connect Server Configuration
Request for an OpenID Connect client at the OpenID Provider. Provide the following details:

1. login redirect uri = this-app/gluu/login.php
2. logout redirect uri = this-app/gluu/logout.php
3. authentication method = client_secret_basic
4. requested scopes = openid email
5. response type = code
6. signing algorithms = any HMAC-SHA-based algorithms (HS256)
7. (optional) session management logout uri = this-app/gluu/logout.php

RSA-based algorithms are currently not supported, but may be added in the future.

Take note of the following details provided by the OpenID Provider:

1. client id
2. client secret
3. OpenID server url


## Zimbra Configuration
Configure a Zimbra domain to enable preauthentication, following instructions in [the Zimbra wiki](https://wiki.zimbra.com/wiki/Preauth).

For example, to enable preauthentication for the domain DOMAIN.COM

```
zmprov gdpak DOMAIN.COM
```

Take note of the generated preauthentication key.

Then, in the admin interface, go to Configure -> Domains -> Select Domain and select the DOMAIN.COM domain.

1. Under authentication, edit "Web Client Login Redirect URL" to be this-app/zimbra/login.php
2. Under authentication, edit "Web Client Logout Redirect URL" to be this-app/zimbra/logout.php
3. Restart the zimbra server

You may optionally configure auto-provisioning to pull user info from the user database of the OpenID server. See [the Zimbra documentation](https://www.zimbra.com/docs/ne/8.6.0/administration_guide/wwhelp/wwhimpl/js/html/wwhelp.htm#href=860_admin_ne.Auto_Provisioning_New_Accounts_from_External_LDAP.html) on autoprovisioning for guidance.


## Middleware configuration (this app)
This application needs to store OpenID Connect sessions in a database. There is a provided SQL script that creates the oic_sessions table for MySQL. Edit the SQL script if desired to support other databases, then create the database.

```
$ mysql -u root
MySQL> CREATE DATABASE gluu_zimbra_sso;
MySQL> USE gluu_zimbra_sso;
MySQL> SOURCE oic_sessions.sql;
```

Next, copy config/config.php.example to config/config.php and edit to add the necessary details.

1. baseurl = url of this middleware
2. database = connection string in the format mysql://username:password@hostname/database
3. openid_connect
    1. url = URL of OpenID Provider
    2. client_id, client_secret = credentials received from OpenID Provider
4. zimbra
    1. url = URL of Zimbra
    2. preauth_key = preauthentication key received from zmprov


## How it works
Upon visiting the Zimbra web client, you should now be redirected to the middleware application which will redirect you to the OpenID provider.

Upon successful login, the OpenID provider will redirect back to the middleware application which will generate the credentials for the redirect to the Zimbra webapp.

If a logout is initiated from Zimbra, you will be redirected to the middleware application which will generate the logout request in the OpenID provider.

If a logout is initiated by OpenID Connect Session management, the /gluu/logout.php iframe will destroy the local session. Zimbra itself, however, has limited support for OP-initiated logout.
