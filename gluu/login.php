<?php
	// called by gluu to redirect to zimbra
	require_once dirname(dirname(__FILE__)) . '/lib/init.php';
	
	// Establish a valid OpenID Connect Session
	if (empty($_SESSION['oic_session_id'])) {
		header("HTTP/1.1 401 Unauthorized");
		exit('<a href="' . appUrl('zimbra/login.php') . '">Client must login.</a>');
	}
	
	try {
		$oic_session = OicSession::find($_SESSION['oic_session_id']);
	} catch (ActiveRecord\RecordNotFound $e) {
		unset($_SESSION['oic_session_id']);
		header("HTTP/1.1 401 Unauthorized");
		exit('Login session expired. <a href="' . appUrl('/zimbra/login.php') . '">Client must reauthorize.</a>');
	}
	
	if ($oic_session->isComplete() && $oic_session->isExpired()) {
		try {
			$oic_session->refreshAccessToken();
		} catch (OicErrorException $e) {
			$oic_session->delete();
			unset($_SESSION['oic_session_id']);
			header("HTTP/1.1 401 Unauthorized");
			exit('Login session expired. <a href="' . appUrl('/zimbra/login.php') . '">Client must reauthorize.</a>');
		}
	}
	
	if (empty($_REQUEST['code'])) {
		render('login.php');
		exit();
	}
	
	if ($oic_session->attributes()['state'] != $_REQUEST['state']) {
		$oic_session->delete();
		unset($_SESSION['oic_session_id']);
		header("HTTP/1.1 401 Unauthorized");
		exit('Invalid OpenID Connect request. <a href="' . appUrl('/zimbra/login.php') . '">Click here to reauthorize.</a>');
	}
	
	$oic_session->set_attributes([
		'code' => $_REQUEST['code'],
		'session_state' => $_REQUEST['session_state'],
	]);
	if (!empty($_REQUEST['id_token'])) {
		$oic_session->set_attributes(['id_token' => $_REQUEST['id_token']]);
		if (!$oic_session->validateIdToken()) {
			$oic_session->delete();
			unset($_SESSION['oic_session_id']);
			header("HTTP/1.1 401 Unauthorized");
			exit('Invalid ID Token. <a href="' . appUrl('/zimbra/login.php') . '">Click here to reauthorize.');
		}
	}
	
	try {
		$oic_session->getAccessToken();
	} catch (OicException $e) {
		$oic_session->delete();
		unset($_SESSION['oic_session_id']);
		header("HTTP/1.1 401 Unauthorized");
		exit('Error while retrieving authorization: ' . $e->getMessage() . '. <a href="' . appUrl('/zimbra/login.php') . '">Click here to reauthorize</a>');
	}
	
	// OpenID Connect session established
	
	// Create a local login session
	$email = $oic_session->getClaim('email');
	$expires_at = $oic_session->attributes()['expires_at'];
	if (!empty($expires_at)) {
		$expiry = (new DateTime($expires_at))->format('U') * 1000;
	} else {
		$expiry = (new DateTime())->format('U') * 1000 + 300000;
	}
	
	$oic_session->save();
		
	$preauth = new ZimbraPreauth($email, $expiry);
	header('Location: ' . $preauth->getRedirectUrl());
