<?php
	require_once dirname(__FILE__) . '/lib/common.php';
?>
Home Page
<hr />
<?php
print_r($_SESSION);
?>
<?php if (empty($_SESSION['user'])) { ?>
	<p>Not yet logged in. <a href="zimbra/login.php">Log in?</a></p>
<?php } else { ?>
	<p>Logged in as <?= $_SESSION['user']['given_name'] . ' ' . $_SESSION['user']['family_name']; ?></p>
	<p><a href="zimbra/logout.php">Log out?</a></p>
<?php } ?>
