<?php
	// called by zimbra to redirect to gluu
	require_once __DIR__ . '/../lib/init.php';

	if (empty($_SESSION['oic_session_id'])) {
		$oic_session = new OicSession;
		$oic_session->save();
		
		$_SESSION['oic_session_id'] = $oic_session->attributes()['id'];
	} else {
		try {
			$oic_session = OicSession::find($_SESSION['oic_session_id']);
		} catch (ActiveRecord\RecordNotFound $e) {
			$oic_session = new OicSession;
		}

		if ($oic_session->isComplete() && $oic_session->isExpired()) {
			try {
				$oic_session->refreshAccessToken();
			} catch (OicErrorException $e) {
				$oic_session->delete();
				$oic_session = new OicSession;
			}
		}
		$oic_session->save();
		
		$_SESSION['oic_session_id'] = $oic_session->attributes()['id'];
	}
	
	if ($oic_session->isComplete()) {
		// user already logged in
		$redirect_url = "STUB: call zimbra redirect";
		$email = $oic_session->getClaim('email');
		$expires_at = $oic_session->attributes()['expires_at'];
		if (!empty($expires_at)) {
			$expiry = (new DateTime($expires_at))->format('U') * 1000;
		} else {
			$expiry = (new DateTime())->format('U') * 1000 + 300000;
		}

		$preauth = new ZimbraPreauth($email, $expiry);
		header('Location: ' . $preauth->getRedirectUrl());
		exit();
	} else {
		// user not yet logged in
		$redirect_url = $oic_session->getAuthorizationUrl();
	}
	
	header('Location: ' . $redirect_url);
?>
<a href="<?= "$redirect_url" ?>">Click here if your browser does not redirect you</a>
